
CREATE TABLE `regions` (
  `id` varchar(24) NOT NULL,
  `name` varchar(26) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cultures` (
  `id` varchar(24) NOT NULL,
  `name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `continents` (
  `id` varchar(24) NOT NULL,
  `cardinal_direction` varchar(5) DEFAULT NULL,
  `name` varchar(9) DEFAULT NULL,
  `neighbors` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `cities_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `ages` (
  `id` varchar(24) NOT NULL,
  `end_date` int(11) DEFAULT NULL,
  `name` varchar(23) DEFAULT NULL,
  `predecessor` varchar(24) DEFAULT NULL,
  `start_date` int(11) DEFAULT NULL,
  `successor` varchar(24) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `events` (
  `id` varchar(24) NOT NULL,
  `age` varchar(24) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `name` varchar(39) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `age` (`age`),
  CONSTRAINT `events_ibfk_1` FOREIGN KEY (`age`) REFERENCES `ages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `characters` (
  `id` varchar(24) NOT NULL,
  `actor` varchar(16) DEFAULT NULL,
  `culture` varchar(24) DEFAULT NULL,
  `date_of_birth` int(11) DEFAULT NULL,
  `date_of_death` int(11) DEFAULT NULL,
  `father` varchar(24) DEFAULT NULL,
  `heir` varchar(31) DEFAULT NULL,
  `house` varchar(39) DEFAULT NULL,
  `image_link` varchar(70) DEFAULT NULL,
  `mother` varchar(24) DEFAULT NULL,
  `name` varchar(42) DEFAULT NULL,
  `page_rank` float DEFAULT NULL,
  `slug` varchar(42) DEFAULT NULL,
  `spouse` varchar(36) DEFAULT NULL,
  `titles` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `culture` (`culture`),
  KEY `spouse` (`spouse`),
  KEY `father` (`father`),
  KEY `heir` (`heir`),
  KEY `mother` (`mother`),
  KEY `house` (`house`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `houses` (
  `id` varchar(24) NOT NULL,
  `ancestral_weapon` varchar(12) DEFAULT NULL,
  `cadet_branch` varchar(33) DEFAULT NULL,
  `coat_of_arms` varchar(466) DEFAULT NULL,
  `current_lord` varchar(25) DEFAULT NULL,
  `founded` int(11) DEFAULT NULL,
  `image_link` varchar(63) DEFAULT NULL,
  `name` varchar(39) DEFAULT NULL,
  `overlord` varchar(33) DEFAULT NULL,
  `region` varchar(24) DEFAULT NULL,
  `title` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `region` (`region`),
  KEY `current_lord` (`current_lord`),
  KEY `overlord` (`overlord`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sentiments` (
  `id` varchar(24) NOT NULL,
  `characters` varchar(32) DEFAULT NULL,
  `neg_count` int(11) DEFAULT NULL,
  `neg_sum` int(11) DEFAULT NULL,
  `null_count` int(11) DEFAULT NULL,
  `pos_count` int(11) DEFAULT NULL,
  `pos_sum` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `characters` (`characters`),
  CONSTRAINT `sentiments_ibfk_1` FOREIGN KEY (`characters`) REFERENCES `characters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `characters`
ADD (CONSTRAINT `characters_ibfk_1` FOREIGN KEY (`culture`) REFERENCES `cultures` (`id`),
    CONSTRAINT `characters_ibfk_2` FOREIGN KEY (`spouse`) REFERENCES `characters` (`id`),
    CONSTRAINT `characters_ibfk_3` FOREIGN KEY (`father`) REFERENCES `characters` (`id`),
    CONSTRAINT `characters_ibfk_4` FOREIGN KEY (`heir`) REFERENCES `characters` (`id`),
    CONSTRAINT `characters_ibfk_5` FOREIGN KEY (`mother`) REFERENCES `characters` (`id`),
    CONSTRAINT `characters_ibfk_6` FOREIGN KEY (`house`) REFERENCES `houses` (`id`));

ALTER TABLE `houses`
ADD (CONSTRAINT `houses_ibfk_1` FOREIGN KEY (`region`) REFERENCES `regions` (`id`),
    CONSTRAINT `houses_ibfk_2` FOREIGN KEY (`current_lord`) REFERENCES `characters` (`id`),
    CONSTRAINT `houses_ibfk_3` FOREIGN KEY (`overlord`) REFERENCES `houses` (`id`));